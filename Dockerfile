# specify a suitable source image
FROM node:latest

WORKDIR /app

COPY package*.json ./

RUN npm ci

# copy the application source code files
COPY *.js /app/

EXPOSE 3000

# specify the command which runs the application
CMD [ "npm", "start" ]